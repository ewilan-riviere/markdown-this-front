import Vue from 'vue'
import Router from 'vue-router'

import Home from '~/pages/index'
// import Login from '~/pages/login'
import Dashboard from '~/pages/dashboard'
import Login from '~/pages/login'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        name: 'home',
        path: '/',
        component: Home,
      },
      {
        name: 'login',
        path: '/login',
        component: Login,
      },
      {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard,
      },
      // {
      //   name: 'Login',
      //   path: '/login',
      //   component: Login,
      // },
    ],
    scrollBehavior(to) {
      if (to.hash) {
        return window.scrollTo({
          top: document.querySelector(to.hash).offsetTop,
          behavior: 'smooth',
        })
      }
      return window.scrollTo({ top: 0, behavior: 'smooth' })
    },
  })
}
