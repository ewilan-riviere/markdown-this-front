require('dotenv').config()

export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'MarkdownThis!',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/css/app'],

  loading: {
    color: '#800080',
    height: '5px',
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '~/plugins/icons-loader',
    {
      src: '~/plugins/vue-tailwind-screens',
      ssr: false,
    },
    '~/plugins/axios',
    // https://github.com/ndelvalle/v-click-outside
    '~/plugins/v-click-outside',
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: [
    '~/components/global',
    {
      path: '~/components/global',
      global: true,
      prefix: 'global',
      extensions: ['vue'],
    },
  ],

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/router-module
    '@nuxtjs/router',
  ],

  tailwindcss: {
    cssPath: '~/assets/css/tailwind.css',
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://auth.nuxtjs.org
    '@nuxtjs/auth-next',
    // https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
    // https://github.com/nuxt-community/markdownit-module
    '@nuxtjs/markdownit',
  ],

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    use: ['markdown-it-div', 'markdown-it-attrs', 'markdown-it-highlight'],
    injected: true,
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  // axios: {
  // proxy: true,
  // credentials: true
  // },
  // proxy: {
  //   '/laravel': {
  //     target: 'https://laravel-auth.nuxtjs.app',
  //     pathRewrite: { '^/laravel': '/' }
  //   }
  // },
  // auth: {
  //   strategies: {
  //     laravelSanctum: {
  //       provider: 'laravel/sanctum',
  //       url: 'http://127.0.0.1:8000',
  //     },
  //   },
  // },
  axios: {
    baseURL: process.env.API_URL,
    prefix: '/api',
    credentials: true,
    https: false,
    // proxy: true,
    headers: {
      common: {
        'X-Requested-With': 'XMLHttpRequest',
      },
      // delete: {},
      // get: {},
      // head: {},
      // post: {},
      // put: {},
      // patch: {},
    },
  },

  // proxy: {
  //   '/api/': { target: 'http://api.example.com', pathRewrite: {'^/api/': ''} }
  // },

  auth: {
    strategies: {
      laravelSanctum: {
        provider: 'laravel/sanctum',
        url: process.env.API_URL,
      },
    },
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/',
      home: '/dashboard',
    },
  },

  // auth: {
  //   strategies: {
  //     laravelSanctum: {
  //       provider: 'laravel/sanctum',
  //       url: process.env.API_URL,
  //     },
  //   },
  // },
  // auth: {
  //   // plugins: ['@/plugins/axios.js'],
  //   strategies: {
  //     // cookie: {
  //     //   cookie: {
  //     //     name: 'X-XSRF-TOKEN',
  //     //   },
  //     // },
  //     laravelSanctum: {
  //       provider: 'laravel/sanctum',
  //       url: process.env.apiUrl,
  //     },
  //     local: {
  //       endpoints: {
  //         login: { url: '/api/login', method: 'post', propertyName: false },
  //         logout: { url: '/api/logout', method: 'post', propertyName: false },
  //         user: { url: '/api/user', method: 'get', propertyName: false },
  //       },
  //       tokenRequired: false,
  //       tokenType: false,
  //     },
  //   },
  // redirect: {
  //   login: '/login',
  //   logout: '/login',
  //   callback: '/login',
  //   home: '/',
  // },
  // },

  router: {
    middleware: ['auth'],
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
