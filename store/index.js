export const state = () => ({
  sidebarOpened: false,
  layerVisible: false,
  authToken: false,
})

export const mutations = {
  toggleSidebarOpened(state, data) {
    state.sidebarOpened = !state.sidebarOpened
  },
  setLayerVisible(state, data) {
    state.layerVisible = data
  },
}
