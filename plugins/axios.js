// export default function ({ $axios, $store }) {
//   console.log('AXIOS PLUGIN LOADED')
//   $axios.onRequest((config) => {
//     config.headers.common['X-Requested-With'] =
//       $store.state.user.headers.xml_http_request
//   })
// }

// export default function ({ $axios, redirect }) {
//   $axios.onRequest((config) => {
//     config.withCredentials = true

//     return config
//   })
// }

import Vue from 'vue'
// eslint-disable-next-line import/no-duplicates
import axios from 'axios'

// console.log('axios')
// export default ({ $axios, env }) => {
//   $axios.onRequest((config) => {
//     console.log('config')
//     config.headers.common['X-Requested-With'] = 'XMLHttpRequest'
//     config.withCredentials = true
//   })
// }

const http = axios.create({
  baseURL: process.env.API_URL,
  withCredentials: true,
  // headers: {
  //   common: {
  //     'X-Requested-With': 'XMLHttpRequest',
  //   },
  // },
})

Vue.prototype.$http = http

export default function ({ $axios, redirect }) {
  $axios.onError((error) => {
    if (error.response.status === 500) {
      redirect('/sorry')
    }
  })
}

// base.defaults.baseURL = process.env.API_URL
// base.defaults.withCredentials = true
// base.defaults.headers.common = {
//   'X-Requested-With': 'XMLHttpRequest',
// }

// Vue.prototype.$base = base

// Vue.prototype.$axiosHeader = axios

// import store from '@/store/index'

// export default function ({ $axios, app }) {
//   $axios.onRequest((config) => {
// if (store.state.authToken) {
//   config.headers.common.Authorization = store.state.authToken
// }
// headers: { 'X-Requested-With': 'XMLHttpRequest' },
//     config.headers.common.XRequestedWith = 'XMLHttpRequest'
//   })
// }

// export default function ({ $axios, store, redirect }) {
// console.log('AXIOS PLUGIN LOADED')

//   $axios.onRequest((request) => {
//     console.log('[ REQUEST ]' + request.url)
//     if (store.state.sessionStorage.authUser) {
//       request.headers.common.Authorization =
//         store.state.sessionStorage.authUser.token_type +
//         ' ' +
//         store.state.sessionStorage.authUser.access_token
//     }

//     return request
//   })

//   $axios.onResponse((response) => {
//     console.log('[ RESPONSE ]' + response.request.responseURL, response)
//     // TODO: If token expires, perform a silent refresh

//     return response
//   })

//   $axios.onError((error) => {
//     console.error('[ ERROR ]', error)
//     const code = parseInt(error.response && error.response.status)
//     if (code === 401) {
//       store.state.sessionStorage.authUser = null
//       return redirect('/')
//     }

//     return error
//   })
// }
