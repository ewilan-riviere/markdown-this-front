import Api from './Api'
import Csrf from './Csrf'

// export default {
//   register(form) {
//     return Api().post('/register', form)
//   },

//   login(form) {
//     return Api().post('/login', form)
//   },

//   logout() {
//     return Api().post('/logout')
//   },

//   auth() {
//     return Api().get('/user')
//   },
// }

export default {
  async register(form) {
    await Csrf.getCookie()
    return Api().post('/register', form)
  },

  login(form) {
    console.log(form)
    return Api().post('/login', form)
  },

  async logout() {
    await Csrf.getCookie()
    return Api().post('/logout')
  },

  auth() {
    return Api().get('/user')
  },
}
