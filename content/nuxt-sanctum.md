# NuxtJS with Sanctum

- [**stackoverflow.com/x-xsrf-token-header-with-axios**](https://stackoverflow.com/questions/52863990/x-xsrf-token-header-with-axios) : configuration of axios directly with call
- [**dev.auth.nuxtjs.org/providers/laravel-sanctum**](https://dev.auth.nuxtjs.org/providers/laravel-sanctum/) : documentation of nuxt/auth, next version
- [**morioh.com**](https://morioh.com/p/f053024505ab) : vidéo with Laravel Airlock and Vue SPA
- [**axios.nuxtjs.org/options**](https://axios.nuxtjs.org/options) : NuxtJS documentation for Axios module
- [**medium.com/frianbiz/laravel-airlock-vue-js**](https://medium.com/frianbiz/laravel-airlock-vue-js-f08724b64da2) : tuto Laravel Airlock with Vue.js
- GitHub example
  - [github.com/garethredfern/sanctum-vue](https://github.com/garethredfern/sanctum-vue)
  - [github.com/serkandyck/laravel-sanctum-vue-spa](https://github.com/serkandyck/laravel-sanctum-vue-spa)

```js
import Vue from 'vue'
import VueMarkdown from 'vue-markdown'

import 'simplemde/dist/simplemde.min.css'
import 'highlight.js/styles/atom-one-dark.css'

Vue.component('vue-markdown', VueMarkdown)
```
